from setuptools import setup, find_packages
import os

version = '0.1'

here = os.path.abspath(os.path.dirname(__file__))

requires = [
    'pyyaml',
    'couchdb',
    'requests'
]
test_requires = requires + [

]

entry_points = {
    'console_scripts': [
        'labot_worker = openregistry.labot.worker.labot_worker.worker:main'
    ]
}


setup(name='openregistry.labot.worker',
      version=version,
      description="openregistry.labot.worker",
      classifiers=[
         "Framework :: Pylons",
         "License :: OSI Approved :: Apache Software License",
         "Programming Language :: Python",
         "Topic :: Internet :: WWW/HTTP",
         "Topic :: Internet :: WWW/HTTP :: WSGI :: Application"
      ],
      keywords='web services',
      author='Quintagroup, Ltd.',
      author_email='info@quintagroup.com',
      url="https://github.com/openprocurement/",
      license='Apache License 2.0',
      packages=find_packages(exclude=['ez_setup']),
      include_package_data=True,
      zip_safe=False,
      install_requires=requires,
      tests_require=test_requires,
      extras_require={'test': test_requires},
      entry_points=entry_points,
      )
